## Prerequisites

- [node.js](http://nodejs.org) Node 6 with NPM 3 is required.
- [gulp](http://gulpjs.com/) `npm install -g gulp`
- [git](https://git-scm.com/downloads) git cmd tool is required

## Create App

```shell
git clone https://zdenek-bilek@bitbucket.org/zdenek-bilek/catenamedia-homework.git catenamedia-homework
cd catenamedia-homework
npm install
```

## Start Development

- run `gulp`
- point your browser to [localhost:8000](http://localhost:8000)

## Dev Tasks

- `gulp` run web app in development mode
- `gulp -p` run web app in production mode
- `gulp eslint` eslint

## Production Tasks

- `gulp build -p` build app for production
- `node src/server` start app, remember to set NODE_ENV and SERVER_URL
- `gulp to-html` render app to HTML
