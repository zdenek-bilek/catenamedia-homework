import './Todo.scss';
import Component from 'react-pure-render/component';
import React, { PropTypes } from 'react';
import cx from 'classnames';
import { Col, ButtonToolbar, Button, Clearfix } from 'react-bootstrap';

// Presentational component.
export default class Todo extends Component {

  static propTypes = {
    todo: PropTypes.object.isRequired,
    updateTodo: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.updateTodo = this.updateTodo.bind(this);
    this.removeClick = this.removeClick.bind(this);
    this.doneClick = this.doneClick.bind(this);
    this.todoClick = this.todoClick.bind(this);
  }

  updateTodo() {
    const { todo, updateTodo } = this.props;
    updateTodo({
      ...todo.toJS(),
      status: 'done'
    });
  }

  removeClick() {
    const { todo, updateTodo } = this.props;
    updateTodo({
      ...todo.toJS(),
      status: 'removed',
      showActions: false
    });
  }

  doneClick() {
    const { todo, updateTodo } = this.props;
    updateTodo({
      ...todo.toJS(),
      status: 'done',
      showActions: false
    });
  }

  todoClick() {
    const { todo, updateTodo } = this.props;
    updateTodo({
      ...todo.toJS(),
      showActions: !todo.showActions
    });
  }

  render() {
    const { todo } = this.props;

    return (
      <li className={`${cx({ active: todo.showActions })} ${todo.status}`}>
        <Col md={8} sm={8} xs={7} onClick={this.todoClick} className="todoDescription">
          {todo.description}
        </Col>
        <Col md={4} sm={4} xs={5}>
          <ButtonToolbar className="pull-right">
            {todo.status === 'todo' ?
              <Button onClick={this.doneClick} className="btn-done" /> : null}
            {todo.status !== 'removed' ?
              <Button onClick={this.removeClick} className="btn-remove" /> : null}
          </ButtonToolbar>
        </Col>
        <Clearfix />
      </li>
    );
  }

}
