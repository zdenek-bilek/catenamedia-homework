import { updateTodo } from '../../common/todos/actions';
import Component from 'react-pure-render/component';
import React, { PropTypes } from 'react';
import Todo from './Todo.react';
import { connect } from 'react-redux';

export class Todos extends Component {

  static propTypes = {
    todos: PropTypes.object.isRequired,
    updateTodo: PropTypes.func.isRequired,
    filter: PropTypes.string.isRequired
  };

  render() {
    const { todos, updateTodo, filter } = this.props;

    if (!todos.size) {
      return <p>No todos</p>;
    }

    const list = todos.toList().sortBy(item => item.updated_at).reverse()
      .filter(value => {
        if (filter === 'all') return true;
        return value.status === filter;
      });

    return (
      <ul className="todos">
        {list.map(todo =>
          <Todo
            todo={todo}
            updateTodo={updateTodo}
            key={todo.id}
          />
        )}
      </ul>
    );
  }

}

export default connect(state => ({
  todos: state.todos.map,
  filter: state.todos.filterBy
}), {
  updateTodo
})(Todos);
