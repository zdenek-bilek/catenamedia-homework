import Component from 'react-pure-render/component';
import Helmet from 'react-helmet';
import NewTodo from './NewTodo.react';
import React from 'react';
import Todos from './Todos.react';
import Filter from './Filter.react';
import { Grid } from 'react-bootstrap';

export default class TodosPage extends Component {

  render() {
    return (
      <Grid className="todos-page">
        <Helmet title="Todos" />
        <h2>My Todos</h2>
        <NewTodo />
        <Filter />
        <Todos />
      </Grid>
    );
  }
}
