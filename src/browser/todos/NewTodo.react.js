import './NewTodo.scss';
import { addTodo } from '../../common/todos/actions';
import Component from 'react-pure-render/component';
import React, { PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { reduxForm, getValues } from 'redux-form';

const validate = values => {
  const errors = {};

  if (!values.todo) {
    errors.todo = 'Required';
  } else if (values.todo.length > 120) {
    errors.todo = 'Must be 120 characters or less';
  }

  return errors;
};

const fields = ['todo'];

class NewTodo extends Component {

  static propTypes = {
    addTodo: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    valid: PropTypes.bool.isRequired,
    newTodoForm: PropTypes.object,
    resetForm: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { addTodo, newTodoForm, resetForm } = this.props;
    addTodo(getValues(newTodoForm).todo);
    resetForm();
  }

  render() {
    const { fields: { todo }, valid } = this.props;

    return (
      <div className="newTodo">
        <form onSubmit={this.handleSubmit}>
          <input type="text" maxLength="120" placeholder="Add new task..." {...todo} />
          <Button
            type="submit"
            disabled={!valid}
          />
        </form>
      </div>
    );
  }

}

export default reduxForm({
  form: 'newTodo',
  fields,
  validate
}, state => ({
  newTodoForm: state.form.newTodo
}), {
  addTodo
})(NewTodo);
