import './Filter.scss';
import { updateFilter } from '../../common/todos/actions';
import Component from 'react-pure-render/component';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem } from 'react-bootstrap';

export class Filter extends Component {

  static propTypes = {
    filter: PropTypes.string,
    updateFilter: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.handleFilter = this.handleFilter.bind(this);
  }

  handleFilter(key) {
    const { updateFilter } = this.props;
    updateFilter(key);
  }

  render() {
    const { filter } = this.props;

    return (
      <Nav
        bsClass="filterNav"
        className="filter"
        bsStyle="pills"
        activeKey={filter}
        onSelect={this.handleFilter}
      >
        <NavItem eventKey="all">All</NavItem>
        <NavItem eventKey="todo">To do</NavItem>
        <NavItem eventKey="done">Done</NavItem>
        <NavItem eventKey="removed">Removed</NavItem>
      </Nav>
    );
  }

}

export default connect(state => ({
  filter: state.todos.filterBy
}), {
  updateFilter
})(Filter);
