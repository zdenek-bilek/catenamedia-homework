import App from './app/App.react';
import NotFound from './notfound/NotFoundPage.react';
import React from 'react';
import Todos from './todos/TodosPage.react';
import { IndexRoute, Route } from 'react-router';

export default function createRoutes() {
  return (
    <Route component={App} path="/">
      <IndexRoute component={Todos} />
      <Route component={NotFound} path="*" />
    </Route>
  );
}
