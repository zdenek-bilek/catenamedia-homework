import './App.scss';
import Component from 'react-pure-render/component';
import Footer from './Footer.react';
import Header from './Header.react';
import Helmet from 'react-helmet';
import React, { PropTypes } from 'react';
import { locationShape } from 'react-router';
import { start } from '../../common/app/actions';
import { connect } from 'react-redux';
import { Clearfix } from 'react-bootstrap';

// v4-alpha.getbootstrap.com/getting-started/introduction/#starter-template
const bootstrap4Metas = [
  { charset: 'utf-8' },
  {
    name: 'viewport',
    content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
  },
  {
    'http-equiv': 'x-ua-compatible',
    content: 'ie=edge'
  }
];

class App extends Component {

  static propTypes = {
    children: PropTypes.object.isRequired,
    location: locationShape,
    start: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { start } = this.props;
    start();
  }

  render() {
    const { children, location } = this.props;

    return (
      <div className="main">
        <Helmet
          titleTemplate="%s - Catenamedia Homework"
          meta={[
            ...bootstrap4Metas,
            {
              name: 'description',
              content: 'content...'
            },
          ]}
          link={[
            { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' },
            { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,300' },
          ]}
        />
        {/* Pass location to ensure header active links are updated. */}
        <Header location={location} />
        {children}
        <Footer />
        <Clearfix />
      </div>
    );
  }

}

export default connect(null, {
  start
})(App);
