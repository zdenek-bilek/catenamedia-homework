import Component from 'react-pure-render/component';
import React from 'react';
import { Grid } from 'react-bootstrap';

export default class Footer extends Component {

  render() {
    return (
      <footer>
        <Grid>
          2015 Copyright TASKMASTER
        </Grid>
      </footer>
    );
  }

}
