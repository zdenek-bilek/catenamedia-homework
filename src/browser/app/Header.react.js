import Component from 'react-pure-render/component';
import React from 'react';
import { Grid, Col } from 'react-bootstrap';

export default class Header extends Component {

  render() {
    return (
      <header>
        <Grid>
          <Col>
            <h1>
              <img src={require('../assets/images/logo.png')} alt="Logo" />
              <span>Taskmaster 5000</span>
            </h1>
          </Col>
        </Grid>
      </header>
    );
  }

}
