import Component from 'react-pure-render/component';
import Helmet from 'react-helmet';
import React from 'react';
import { Link } from 'react-router';

export default class NotFoundPage extends Component {

  render() {
    return (
      <div className="notfound-page">
        <Helmet title="Page Not Found" />
        <h1>
          This page isn't available
        </h1>
        <p>
          The link may be broken, or the page may have been removed.
        </p>
        <Link to="/">
          Continue here please.
        </Link>
      </div>
    );
  }

}
