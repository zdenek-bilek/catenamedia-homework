import { Record } from '../transit';

const Todo = Record({
  updated_at: null,
  id: '',
  description: '',
  status: '',
  showActions: false
}, 'todo');

export default Todo;
