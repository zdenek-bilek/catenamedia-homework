export const ADD_TODO = 'ADD_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const UPDATE_FILTER = 'UPDATE_FILTER';

export function addTodo(description) {
  return ({ getUid, now }) => ({
    type: ADD_TODO,
    payload: {
      updated_at: now(),
      id: getUid(),
      description: description.trim(),
      status: 'todo'
    }
  });
}

export function updateTodo(todo) {
  return {
    type: UPDATE_TODO,
    payload: { ...todo }
  };
}

export function updateFilter(filterBy) {
  return {
    type: UPDATE_FILTER,
    payload: filterBy
  };
}
