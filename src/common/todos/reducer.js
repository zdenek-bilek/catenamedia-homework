import * as actions from './actions';
import Todo from './todo';
import { Map } from 'immutable';
import { Record } from '../transit';

const InitialState = Record({
  map: Map(),
  fetched: false,
  filterBy: 'all'
}, 'todos');

export default function todosReducer(state = new InitialState, action) {
  switch (action.type) {

    case actions.ADD_TODO: {
      const todo = new Todo(action.payload);
      return state.update('map', map => map.set(todo.id, todo));
    }

    case actions.UPDATE_TODO: {
      const todo = new Todo(action.payload);
      return state.update('map', map => map.set(todo.id, todo));
    }

    case actions.UPDATE_FILTER: {
      return state.set('filterBy', action.payload);
    }

  }

  return state;
}
