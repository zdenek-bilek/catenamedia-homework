import app from './app/reducer';
import config from './config/reducer';
import device from './device/reducer';
import todos from './todos/reducer';
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { updateStateOnStorageLoad } from './configureStorage';
import { reducer as formReducer } from 'redux-form';


export default function configureReducer(initialState, platformReducers) {
  let reducer = combineReducers({
    ...platformReducers,
    app,
    config,
    device,
    routing,
    todos,
    form: formReducer
  });

  // The power of higher-order reducers, http://slides.com/omnidan/hor
  reducer = updateStateOnStorageLoad(reducer);

  return reducer;
}
